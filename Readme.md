Docker debug-tools
==================

This Docker image is a simple container which just has debugging tools I need generally.

The intention is for it to be included in a docker-compose file on the same network as other containers you want to
probe.

It will change often (by having new tools added) as I realise I wish I had another tool in there.
