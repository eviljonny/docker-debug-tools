# This Dockerfile is intended to provide a container which has a bunch of useful
# tools I use for general debugging and exploration. 
FROM ubuntu:16.04

MAINTAINER Jonathan Harden <jfharden@gmail.com>

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
      awscli \
      dnsutils \
      groff \
      net-tools \
      iputils-ping \
      telnet \
      vim && \
    rm -rf /var/lib/apt/lists/*

CMD /bin/bash
